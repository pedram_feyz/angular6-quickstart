import { NgModule } from '@angular/core';
// You'll configure the router with Routes in the RouterModule
import { RouterModule, Routes } from '@angular/router';

import { HeroesComponent }      from './heroes/heroes.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { HeroDetailComponent }  from './hero-detail/hero-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'heroes', component: HeroesComponent},
  { path: 'dashboard', component: DashboardComponent},
  { path: 'detail/:id', component: HeroDetailComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ], // You first must initialize the router and start it listening for browser location changes.
  exports:[ RouterModule ]  // Exporting RouterModule makes router directives available for use in the AppModule components
})
export class AppRoutingModule { 
  
}


